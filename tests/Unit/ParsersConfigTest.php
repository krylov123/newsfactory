<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\ParsersConfig;

class ParsersConfigTest extends TestCase
{

    private $parsersConfig;

    protected function setUp(): void
    {
        $this->parsersConfig = ParsersConfig::getInstance();
    }

    public function test_is_instance_correct()
    {
        $this->assertTrue($this->parsersConfig instanceof ParsersConfig);
    }

    public function test_get_config_return_array()
    {
        $this->assertTrue(is_array($this->parsersConfig->getConfig()));
    }

    public function test_get_feeds_return_array()
    {
        $this->assertTrue(is_array($this->parsersConfig->getFeeds()));
    }
}
