<?php

return [
    'feeds' => [
        [
            'url'         => 'https://lenta.ru/rss/news',
            'tag'         => 'lenta-coronavirus',
            'feedFetcher' => \App\Fetchers\CoronavirusRssFetcher::class,
        ],
        [
            'url'         => 'https://www.mk.ru/rss/index.xml',
            'tag'         => 'mk-coronavirus',
            'feedFetcher' => \App\Fetchers\CoronavirusRssFetcher::class,
        ],
        [
            'url'         => 'https://lenta.ru/rss/news',
            'tag'         => 'lenta-elon-musk',
            'feedFetcher' => \App\Fetchers\ElonMuskRssFetcher::class,
        ],
        [
            'url'         => 'https://www.mk.ru/rss/index.xml',
            'tag'         => 'mk-elon-musk',
            'feedFetcher' => \App\Fetchers\ElonMuskRssFetcher::class,
        ],
        [
            'url'         => 'https://elementy.ru/rss/news',
            'tag'         => 'elementy-elon-musk',
            'feedFetcher' => \App\Fetchers\ElonMuskRssFetcher::class,
        ],
        [
            'url'         => 'https://www.sciencetoday.ru/cosmos?format=feed&type=rss',
            'tag'         => 'sciencetoday-elon-musk',
            'feedFetcher' => \App\Fetchers\ElonMuskRssFetcher::class,
        ],
        [
            'url'         => 'https://www.popmech.ru/out/public-all.xml',
            'tag'         => 'popmech-elon-musk',
            'feedFetcher' => \App\Fetchers\ElonMuskRssFetcher::class,
        ],
        [
            'url'         => 'https://news.yandex.ru/science.rss',
            'tag'         => 'yandex-elon-musk',
            'feedFetcher' => \App\Fetchers\ElonMuskRssFetcher::class,
        ],
        [
            'url'         => 'https://naked-science.ru/article/category/cosmonautics/feed',
            'tag'         => 'naked-science-elon-musk',
            'feedFetcher' => \App\Fetchers\ElonMuskRssFetcher::class,
        ],
    ]
];
