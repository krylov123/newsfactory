# Newsfactory API

Предоставляет REST API для новостных фронтов

## Установка
`git clone https://krylov123@bitbucket.org/krylov123/newsfactory.git`  
`cd newsfactory`  
`composer install`  
`cp .env.example .env`  
`php artisan key:generate`  

В .env заменить данные секции на свои: 
```
APP_API_DOMAIN=api.coronavir.info  
DB_CONNECTION=mysql  
DB_HOST=127.0.0.1  
DB_PORT=3306  
DB_DATABASE=laravel  
DB_USERNAME=root  
DB_PASSWORD=  
```
Затем запустить БД миграции чтобы создать все таблицы:  
`php artisan migrate`  

Настроить веб сервер чтобы смотрел в `/public` папку репозитория (за пределами данного README)

## Запуск парсеров
Вручную запуск: `php artisan parse:rss`

Либо добавить в планировщик заданий `artisan schedule:run`, тогда парсер будет запускаться каждые 5 минут  

## Запуск тестов

`vendor/bin/phpunit`

## ГДЕ/ЧТО
* Конфиг парсеров в `/config/parsers.php`  
* Для добавления нового парсера, добавляем секцию в конфиг:
```php
[
    'url'         => 'https://naked-science.ru/article/category/cosmonautics/feed',
    'tag'         => 'naked-science-elon-musk',
    'feedFetcher' => \App\Fetchers\ElonMuskRssFetcher::class,
]
```
`url` - это ссылка на RSS фид  
`tag` - это тег под которым будет сохранен результат в БД  
`feedFetcher` - ссылка на класс который должен соответствовать интерфейсу IFetcher, можно указать null для использования дефолтного fetcher'а который будет принимать все новости без фильтрации  
* Классы Request'ов (+валидация) лежат в `/app/Http/Requests/News`  
* Respons'ы (в Laravel через JsonResource) лежат в `/app/Http/Resorces`  
* Основные классы в `/app/`, `/app/Feeds` и `/app/Fetchers`, а модели в `/app/Models`    


##TODO
* Добавить генерацию и выдачу jwt токенов  
* Добавить права на редактирование определенных project'ов только определенным пользователям
* Покрыть код парсеров тестами  
* Избавиться от маппинга названий проектов к тегам моделей (вынести в конфиг, сделать динамическим)  
* Подключить redis для управления очередями  
* При возможности парсить полный текст статьи по ссылке из RSS фида  
* Возможно: Вынести метод получения ссылки с помощью фабричного метода, чтобы брать не только RSS фиды, но и например json формат  
* Прописать кастомные Exception'ы для ошибок в Request'ах

## License

Newsfactory CMS is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
