<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('hash');
            $table->string('guid')->nullable();
            $table->string('title');
            $table->text('preview');
            $table->longText('description')->nullable();
            $table->string('link', 2048)->nullable();
            $table->string('image_link', 2048)->nullable();
            $table->string('category')->nullable();
            $table->date('pub_date');
            $table->string('tag');
            $table->json('json')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
