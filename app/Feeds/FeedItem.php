<?php


namespace App\Feeds;


class FeedItem
{
    protected $guid = "";
    protected $title = "";
    protected $link = "";
    protected $preview = "";
    protected $description = "";
    protected $date = "";
    protected $category = "";
    protected $imageLink = "";

    public function fromArray(array $array): self
    {
        foreach ($array as $key => $item) {
            if (isset($this->{$key})) $this->{$key} = (string)$item;
        }
        return $this;
    }

    public function toArray(): array
    {
        return [
            "guid"        => $this->getGuid(),
            "title"       => $this->getTitle(),
            "link"        => $this->getLink(),
            "preview"     => $this->getPreview(),
            "description" => $this->getDescription(),
            "date"        => $this->getDate(),
            "category"    => $this->getCategory(),
            "image_link"  => $this->getImageLink()
        ];
    }

    public function getGuid(): string
    {
        return $this->guid;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getLink(): string
    {
        return $this->link;
    }

    public function getPreview(): string
    {
        return $this->preview;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getDate(): ?string
    {
        return (empty($this->date) ? null : $this->date);
    }

    public function getCategory(): string
    {
        return $this->category;
    }

    public function getImageLink(): string
    {
        return $this->imageLink;
    }
}
