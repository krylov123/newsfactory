<?php

namespace App;

class ParsersConfig extends Singleton
{

    private $config;

    protected function __construct()
    {
        $this->config = include "./config/parsers.php";
    }

    public function getConfig(): array
    {
        return $this->config ?? [];
    }

    public function getFeeds(): array
    {
        return $this->config["feeds"] ?? [];
    }

}
