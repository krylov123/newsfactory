<?php


namespace App;

use App\Feeds\FeedItem;
use App\Fetchers\IFetcher;

class FeedParser
{
    protected $url;
    protected $fetcher;
    protected $result = null;

    public function __construct(string $url = "", IFetcher $fetcher)
    {
        $this->url = $url;
        $this->fetcher = $fetcher;
    }

    /**
     * Добавляет Item в массив результатов
     * Гарантирует что в массиве будут только FeedItem объекты
     * @param FeedItem $item
     */
    protected function addItem(FeedItem $item)
    {
        $this->result[] = $item;
    }

    /**
     * Запускает fetcher, прогоняет результат по фильтрам и сохраняет в $this->result
     */
    public function parse()
    {
        $this->result = [];
        $fetchedItems = $this->fetcher->fetch($this->url);
        foreach ($fetchedItems as $fetchedItem) {
            $this->addItem($fetchedItem);
        }
    }

    /**
     * Основной метод, возвращает результат в виде массива FeedItem объектов
     * спарсенных из $this->url c помощью IFetcher
     *
     * @return array
     */
    public function getResult(): array
    {
        if (is_null($this->result)) $this->parse();
        return $this->result;
    }
}
