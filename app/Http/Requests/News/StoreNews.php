<?php

namespace App\Http\Requests\Lead;

use Illuminate\Foundation\Http\FormRequest;

class StoreNews extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'guid'        => 'string|nullable',
            'title'       => 'string|required',
            'preview'     => 'string|required',
            'description' => 'string|nullable',
            'link'        => 'string|nullable',
            'image_link'  => 'string|nullable',
            'category'    => 'string|nullable',
            'pub_date'    => 'date|required',
            'tag'         => 'string|required',
        ];
    }
}
