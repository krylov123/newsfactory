<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class News extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = parent::toArray($request);
        return [
            'data'  => $data,
            'links' => [
                'self' => env('APP_URL') . '/news/' . $data['id'],
            ],
        ];
    }
}
