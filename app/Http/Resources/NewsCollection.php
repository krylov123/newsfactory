<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class NewsCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data'  => $this->collection,
            'links' => [
                'self'  => env('APP_URL') . '/news',
                'first' => env('APP_URL') . '/news?page=1',
                'last'  => $this->resource->lastPage(),
                'prev'  => $this->resource->previousPageUrl(),
                'next'  => $this->resource->nextPageUrl()
            ],
            "meta"  => [
                "current_page" => $this->resource->currentPage(),
                "last_page"    => $this->resource->lastPage(),
                "per_page"     => $this->resource->perPage(),
                "total"        => $this->resource->total(),
            ]
        ];
    }
}
