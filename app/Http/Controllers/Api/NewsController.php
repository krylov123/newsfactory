<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\NewsCollection;
use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    const projectTagMap = [
        'coronavirus' => ['lenta-coronavirus', 'mk-coronavirus', 'lenta-elon-musk'],
        'elonmusk'    => ['mk-elon-musk', 'elementy-elon-musk', 'sciencetoday-elon-musk', 'popmech-elon-musk', 'yandex-elon-musk', 'naked-science-elon-musk'],
        'oscar'       => []
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $params = $request->all(); //Если понадобятся фильтры
        if(!isset($params["project"])){
            $models = News::orderBy('id', 'DESC')->paginate(20);
        }else{
            $models = News::whereIn('tag',self::projectTagMap[$params["project"]])->orderBy('id', 'DESC')->paginate(20);
        }

        return response()->json(new NewsCollection($models));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //У нас за это отвечает фронтэнд
        return abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreNews $request)
    {
        $data = $request->validated();
        $news = News::create($data);

        return response()->json($news);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(
            new \App\Http\Resources\News(
                News::findOrFail($id)
            )
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //У нас за это отвечает фронтэнд
        return abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateNews $request, $id)
    {
        $data = $request->validated();

        News::where('id', $id)->update($data);

        return response()->json(News::findOrFail($id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = News::findOrFail($id);
        $news->delete();
    }
}
