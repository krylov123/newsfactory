<?php

namespace App;

abstract class Singleton
{
    private static $instances = []; //Инициализированные объекты храним тут

    protected function __construct()
    {
    }

    protected function __clone()
    {
    }

    public function __wakeup()
    {
        throw new \Exception("Don't do that :)");
    }

    /**
     * Всегда возвращает один и тотже экземпляр класса
     * В первый раз его инициализирует
     * @return mixed
     */
    public static function getInstance()
    {
        $arguments = func_get_args(); //Берем аргументы, чтобы иметь возможность передать их в конструктор потомка

        $childClass = static::class;
        //Если объекта не в массиве $instances, значит он еще не ициализирован
        //Вызываем его конструктор и сохраняем в массив $instances
        if (!isset(self::$instances[$childClass])) {
            self::$instances[$childClass] = new static(...$arguments);
        }
        return self::$instances[$childClass];
    }
}
