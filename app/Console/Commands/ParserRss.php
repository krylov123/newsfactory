<?php

namespace App\Console\Commands;

use App\FeedParser;
use App\Fetchers\DefaultRssFetcher;
use App\Models\News;
use App\ParsersConfig;
use Illuminate\Console\Command;
use Feeds;

class ParserRss extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser:rss';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Парсит указанный RSS url в БД';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $config = ParsersConfig::getInstance();
        $feeds = $config->getFeeds();
        foreach ($feeds as $feed) {
            $fetcherClass = $feed["feedFetcher"] ?? DefaultRssFetcher::class;
            $parser = new FeedParser(
                $feed["url"],
                (new $fetcherClass())
            );
            $result = $parser->getResult();
            $this->saveNewsToDb($result, $feed["tag"] ?? "");
        }
    }

    public function saveNewsToDb(array $array, string $withTag = "")
    {
        foreach ($array as $item) {
            News::findOrCreate($item, $withTag);
        }
    }
}
