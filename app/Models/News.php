<?php

namespace App\Models;

use App\Feeds\FeedItem;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
    use SoftDeletes;

    protected $table = 'news';
    protected $primaryKey = 'id';
    protected $connection = 'mysql';

    protected $attributes = [];

    protected $fillable = ['hash', 'guid', 'title', 'preview', 'description', 'link', 'image_link', 'category', 'pub_date', 'tag', 'json'];

    protected $guarded = [];

    protected $dates = ['pub_date', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * Ищет модель с таким же текстом как в FeedItem
     * Если не находит - создает и сохраняет новую модель
     * @param FeedItem $item
     * @param string $tag
     * @return News
     */
    public static function findOrCreate(FeedItem $item, string $tag = ""): News
    {
        $model = News::where('hash', self::calcHash($item->getTitle() . $item->getDescription() . $tag))
            ->where('tag', $tag)
            ->first();
        if (!is_null($model)) return $model;

        $model = new News();
        $model->tag = $tag;
        $model->fromFeedItem($item)->save();
        return $model;
    }


    /**
     * Заполняет модель данными из FeedItem объекта
     * @param FeedItem $item
     * @return $this
     */
    public function fromFeedItem(FeedItem $item): self
    {
        $array = $item->toArray();
        $this->fill($array);
        $this->pub_date = $array["date"];
        $this->hash = $this->calcHash($this->title . $this->description . $this->tag);
        $this->json = json_encode($item->toArray());
        return $this;
    }

    /**
     * Метод расчета hash модели, по которому проверяем уникальность текста в базе
     * @param string $string
     * @return string
     */
    public static function calcHash(string $string)
    {
        return hash('sha256', $string);
    }
}
