<?php


namespace App\Fetchers;

use App\Feeds\FeedItem;
use Feeds;

class CoronavirusRssFetcher extends DefaultRssFetcher
{

    /**
     * Переопределяем метод чтобы фильтровать новости только с ключевыми словами
     * @param \SimplePie_Item $item
     * @return bool
     */
    public function skipThis(\SimplePie_Item $item): bool
    {
        if (mb_strpos($item->get_title(), "оронавирус") !== false) return false;
        return (mb_strpos($item->get_description(), "оронавирус") === false);
    }

}
