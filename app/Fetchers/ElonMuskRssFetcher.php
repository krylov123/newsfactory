<?php


namespace App\Fetchers;

use App\Feeds\FeedItem;
use Feeds;

class ElonMuskRssFetcher extends DefaultRssFetcher
{
    const WORDS = ["Илон", "SpaceX", "Tesla"];

    /**
     * Переопределяем метод чтобы фильтровать новости только с ключевыми словами
     * @param \SimplePie_Item $item
     * @return bool
     */
    public function skipThis(\SimplePie_Item $item): bool
    {
        foreach (self::WORDS as $word){
            if (mb_strpos($item->get_title(), $word) !== false) return false;
            if (mb_strpos($item->get_description(), $word) !== false) return false;
        }
        return true;
    }

}
