<?php


namespace App\Fetchers;

use App\Feeds\FeedItem;
use Feeds;

class DefaultRssFetcher implements IFetcher
{

    public function fetch(string $url): array
    {
        $result = [];
        $items = Feeds::make($url)->get_items();
        foreach ($items as $item) {
            if($this->skipThis($item)) continue;
            $result[] = (new FeedItem())->fromArray([
                "guid"        => $item->get_id(),
                "title"       => $item->get_title(),
                "link"        => $item->get_link(),
                "preview"     => $item->get_description(),
                "description" => $item->get_description(),
                "date"        => $item->get_date('Y-m-d H:i:s'),
                "category"    => $item->get_category()->term ?? "",
                "imageLink"   => $item->get_enclosure()->link ?? ""
            ]);
        }
        return $result;
    }

    public function skipThis(\SimplePie_Item $item): bool
    {
        return false;
    }

}
