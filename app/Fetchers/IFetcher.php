<?php


namespace App\Fetchers;


interface IFetcher
{
    public function fetch(string $url): array;
}
